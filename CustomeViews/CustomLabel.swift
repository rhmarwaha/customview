//
//  CustomLabel.swift
//  CustomeViews
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import UIKit

class CustomLabel: UILabel{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpLabel(){
        self.textColor = UIColor(named: "myBlue")
        self.font = UIFont(name: "Snell Roundhand", size: 20)
    }
    
}

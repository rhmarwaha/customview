//
//  ViewController.swift
//  CustomeViews
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var loginButton: CustomButton!
    var passwordTextField: CustomTextField!
    var userNameTextField: CustomTextField!
    var logoLabel: CustomLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        
        
        setLoginButton()
        setPasswordTextField()
        setUserNameTextField()
        setLabel()
        
        
        
        
    }
    
    func setLabel(){
        logoLabel = CustomLabel(frame: CGRect(x: 30, y: view.frame.height - 450, width: loginButton.frame.width, height: 100))
        logoLabel.text = "Avengers"
        self.view.addSubview(logoLabel)
    }

    func setLoginButton(){
        loginButton = CustomButton(frame: CGRect(x: 30 , y: view.frame.height - 150  , width: view.frame.width - 60 , height: 50 ))
        loginButton.setTitle("Login", for: .normal)
        self.view.addSubview(loginButton)
    }
    
    func setPasswordTextField(){
            passwordTextField = CustomTextField(frame: CGRect(x: 30, y: view.frame.height - 230 , width: loginButton.frame.width , height: loginButton.frame.height))
        passwordTextField.placeholder = "Password"
              self.view.addSubview(passwordTextField)
    }
    
    func setUserNameTextField(){
        userNameTextField = CustomTextField(frame: CGRect(x: 30, y: view.frame .height - 310 , width: loginButton.frame.width , height: loginButton.frame.height))
        userNameTextField.placeholder = "Username"
            self.view.addSubview(userNameTextField)
    }
}


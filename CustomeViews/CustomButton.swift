//
//  CustomButton.swift
//  CustomeViews
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import UIKit
class CustomButton: UIButton{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpButton(){
        self.backgroundColor = .blue
        self.layer.cornerRadius = self.frame.size.height / 2
        self.setTitleColor(.white, for: .normal)
    }
}

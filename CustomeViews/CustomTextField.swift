//
//  CustomTextField.swift
//  CustomeViews
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation

import UIKit

class CustomTextField: UITextField{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpTextField(){
        self.backgroundColor = .clear
        self.layer.cornerRadius = self.frame.size.height / 2
        self.textColor = UIColor(named: "myBlue")
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(named: "myBlue")?.cgColor
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10 , height: self.frame.height))
        self.leftViewMode = .always
        
        
    }
    
}
